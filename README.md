-----概要-----
GloVeでコーパス走査からモデル構築，Gensimで読めるフォーマットにするところまで
全部一括でやる自作Pythonラッパーのようなもの．

-----プログラムの説明-----
処理自体はStanford本家GloVeのC言語プログラムを使用．
現在使用できるPython実装のGloVeはGensimで使うにはバイナリを読み書きしないと無理なので、Stanfordの本家のC言語実装のGloVeを動かして
Gensimで読み書きできる形に変換する。

なお、一部のパラメータは今のところ調整不要なので、固定にした。

-----パラメーター解説(defaultはC言語実装の方のデフォルト値を表記)------
・引数で調整可能なもの
copus 与えるコーパスファイル。1文1行で問題ないはず(一応要検証)
model_file 出力モデルファイル名
min_count これより登場回数が少ない語は無視(default=5)
window_size ウィンドウサイズ。(default=15)
deimension モデルの次元数(default=50)
x_max 極端に小さいコーパスを使う場合は調整が必要と書いてあった。(default=100)
memory 使用する最大メモリ(単位GB)(default=3)

m_iter 最大の反復数(default=15)

・固定値にしてしまったもの
verbose 学習中に標準出力される内容が変わるらしい(default=2)
binary ファイルの出力形式，2ならテキスト，バイナリの両方のモデルを出力(default=2)

他、中間出力ファイルはすべて名前を固定にし、処理終了後にすべて削除している。
同じディレクトリで複数動かすと互いにファイル読み書きするため複数同時実行は不可能

-------使い方------

StanfordのC言語実装の最新版(v1.2ではエラーが発生してだめ)を解凍してmakeした後、GloVeの
フォルダ内(の1番上の階層)にこのファイルを置けば使用可能。