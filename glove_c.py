# -*- coding:utf-8 -*-
import os
from gensim.scripts import glove2word2vec

def train_glove(copus,model_file,min_count=5,window_size=15,dimension=50,x_max=100,memory=4.0,m_iter=15,cores=4):
	os.system("build/vocab_count -min-count %s -verbose 2 < %s > vocab.tmp" % (min_count,copus))
	os.system("build/cooccur -memory %s -vocab-file vocab.tmp -verbose 2 -window-size %s < %s > coocurance.tmp" % (memory,dimension,copus))
	os.system("build/shuffle -memory %s -verbose 2 < coocurance.tmp > coocurance_suff.tmp" % (memory))
	os.system("build/glove -save-file glove_model.tmp -threads %s -input-file coocurance_suff.tmp -x-max %s -iter %s -vector-size %s -binary 2 -vocab-file vocab.tmp -verbose 2" % (cores,x_max,m_iter,dimension))
	
	glove2word2vec.glove2word2vec("glove_model.tmp.txt",model_file)
	
	os.system("rm *.tmp.*")
	os.system("rm *.tmp")
	